import { Component, OnInit, Input } from "@angular/core";
import { weather } from "../../structures/weather.structure";

@Component({
  selector: "app-weather-card",
  templateUrl: "./weather-card.component.html",
  styleUrls: ["./weather-card.component.scss"],
})
export class WeatherCardComponent implements OnInit {
  @Input() weather: weather;

  constructor() {}

  ngOnInit() {}
}
